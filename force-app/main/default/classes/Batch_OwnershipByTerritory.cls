global class Batch_OwnershipByTerritory implements Database.Batchable<sObject>, Schedulable {
	
    global Batch_OwnershipByTerritory() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT Id, Name, OwnerId, Current_Territory_Ownership__c FROM Territory__c';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Territory__c> scope) {
           Map<Id, Territory__c> territoryMap = new Map<Id, Territory__c>();
           territoryMap.putAll(scope);
           Territory_Handler.updateTerritoryOwnership(territoryMap);
    
    }
	
	global void finish(Database.BatchableContext BC) {
	
    }

	public void execute(SchedulableContext sc) {
	   Database.executeBatch(new Batch_OwnershipByTerritory());
	}	
}