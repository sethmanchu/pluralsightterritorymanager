public class Account_Handler {
    public static void syncAccountOwnerToTerritory(Map<Id, Account> newMap, Map<Id, Account> oldMap) {
        
        //validate Assigned_Territory__c has been updated. Owner change verified in Territory_Handler
        Set<Id> territoryIds = new Set<Id>();
        for (Id a : newMap.keySet()) {
            if (newMap.get(a).Assigned_Territory__c != oldMap.get(a).Assigned_Territory__c) {
                TerritoryIds.add(newMap.get(a).Assigned_Territory__c);
            }
        }
        System.debug(territoryIds);
        Map<Id, Territory__c> territoryMap = new Map<Id, Territory__c>([SELECT Id, OwnerId FROM Territory__c WHERE Id IN : territoryIds]);
        List<Account> accountsToUpdate = Territory_Handler.UpdateAccountOwnership(territoryMap);

        update accountsToUpdate;
    }
}
