public class Territory_Handler {

    //center ownership updates around the Territory Object to give quicker access to both related Ownership and Account records
    public static void updateTerritoryOwnership(Map<Id, Territory__c> territoryMap) {
        Map<Id, Territory_Ownership__c> territoryOwnershipMap = new Map<Id, Territory_Ownership__c>();
        
        //Using a Map here allows update map to be used as param for Account update method
        Map<Id, Territory__c> territoriesToUpdate = new Map<Id, Territory__c>();
        
        for (Territory_Ownership__c newOwnership : [
            SELECT Id, Ownership_Start_Date__c, Ownership_End_Date__c, OwnerId, Territory__c, LastModifiedDate 
            FROM Territory_Ownership__c 
            WHERE Territory__c IN :territoryMap.keySet()
            ]) {

            //pre-populate map so that we can loop through Territory Ownerships comparing each one to the most current record
            //this could be made more efficient by adding a process to mark a Territory Ownership as 'Active' - probably requiring some field validation as well                
            if (!territoryOwnershipMap.containsKey(newOwnership.Territory__c)) {
                territoryOwnershipMap.put(newOwnership.Territory__c, new Territory_Ownership__c());
            }

            Territory_Ownership__c oldOwnership = territoryOwnershipMap.get(newOwnership.Territory__c);
            
            //Calculate which Territory_Ownership__c should be the current owner of the Territory based on Start and End dates.
            //If multiple records fit the criteria, using most recently modified record. Could add additional criteria in future.
            if (newOwnership.Ownership_Start_Date__c != null
                && newOwnership.Ownership_End_Date__c != null
                && newOwnership.Ownership_Start_Date__c < Date.today().addDays(1)
                && newOwnership.Ownership_End_Date__c > Date.today().addDays(-1)) {
                    if (oldOwnership.Id == null || newOwnership.LastModifiedDate > oldOwnership.LastModifiedDate) {
                        territoryOwnershipMap.put(newOwnership.Territory__c, newOwnership);
                    }
            }
        }
        //Prepare Territory__c records that need to be updated
        for (Id territoryId : territoryOwnershipMap.keySet()) {
            Territory_Ownership__c updatedOwnership = territoryOwnershipMap.get(territoryId);
            Territory__c territory = territoryMap.get(territoryId);

            //make sure that the new Territory_Ownership__c record is different than what is currently linked to the Territory__c record
            if (updatedOwnership.Ownership_Start_Date__c != null 
                && (updatedOwnership.OwnerId != territory.OwnerId || updatedOwnership.Id != territory.Current_Territory_Ownership__c)) {
                Territory__c updatedTerritory = new Territory__c(
                    Id = territoryId,
                    OwnerId = updatedOwnership.OwnerId,
                    Current_Territory_Ownership__c = updatedOwnership.Id
                );                
                territoriesToUpdate.put(updatedTerritory.Id, updatedTerritory);
            }
        }
        List<Account> accountsToUpdate = updateAccountOwnership(territoriesToUpdate);
        
        update territoriesToUpdate.values();
        update accountsToUpdate;
    }
    
    // returns a list of Accounts so that this method can be used in various contexts and the update handled as needed
    // using Territory__c Map as parameter makes it simple to update all accounts without multiple method calls
    public static List<Account> updateAccountOwnership(Map<Id, Territory__c> territoryMap) {
        List<Account> accounts = [SELECT Id, OwnerId, Assigned_Territory__c FROM Account WHERE Assigned_Territory__c IN :territoryMap.keySet()];
        List<Account> accountsToUpdate = new List<Account>();
        for (Account a : accounts) {
            // if (a.OwnerId != null && a.OwnerId != territoryMap.get(a.Assigned_Territory__c).OwnerId) {
            if (a.OwnerId != territoryMap.get(a.Assigned_Territory__c).OwnerId) {
                a.OwnerId = territoryMap.get(a.Assigned_Territory__c).OwnerId;
                accountsToUpdate.add(a);
            }
        }
        System.debug(accountsToUpdate);
        return accountsToUpdate;
    }
}