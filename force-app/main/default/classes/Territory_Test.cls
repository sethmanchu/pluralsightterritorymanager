@isTest
public class Territory_Test {
    
    @TestSetup
    static void setupData() {

        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 

        User u = new User(Alias = 'test5', Email='test5@example.com', 
            EmailEncodingKey='UTF-8', LastName='Test5', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@example.com.777'
        );
        insert u;
        
        Territory__c t = new Territory__c(
            Name = 'TestState'
        );
        insert t;

        Territory_Ownership__c to = new Territory_Ownership__c(
            Territory__c = t.Id,
            Ownership_Start_Date__c = Datetime.now().addDays(-1),
            Ownership_End_Date__c = Datetime.now().addDays(1)
        );
        insert to;
        
        Territory__c t1 = new Territory__c(
            Name = 'TestState1',
            OwnerId = u.Id
        );
        insert t1;

        Territory_Ownership__c to1 = new Territory_Ownership__c(
            Territory__c = t1.Id,
            Ownership_Start_Date__c = Datetime.now().addDays(-1),
            Ownership_End_Date__c = Datetime.now().addDays(1)
        );
        insert to1;

        Account a = new Account(
            Name = 'TestAccount',
            Assigned_Territory__c = t.Id
        );
        insert a;
    }

    @isTest
    static void testTerritoryOwnerBatch() {
        User u = [SELECT Id FROM User WHERE Alias = 'test5'];
        Territory_Ownership__c to = [SELECT Id, OwnerId from Territory_Ownership__c LIMIT 1];
        system.debug(to.OwnerId);
        system.debug(u.Id);
        to.OwnerId = u.Id;
        update to;

        Test.startTest();
        Id testTerritoryBatchId = Database.executeBatch(new Batch_OwnershipByTerritory());
        Test.stopTest();

        Account a = [SELECT Id, OwnerId FROM Account LIMIT 1];
        
        System.assert(a.OwnerId == u.Id);
    }

    @isTest
    static void testAccountTerritoryChange() {
        Territory__c t1 = [SELECT Id FROM Territory__c WHERE Name = 'TestState1' LIMIT 1];
        Account a = [SELECT Id FROM Account LIMIT 1];
        a.Assigned_Territory__c = t1.Id;

        Test.startTest();
        update a;
        Test.stopTest();

        User u = [SELECT Id FROM User WHERE Alias = 'test5'];
        Account updatedAccount = [SELECT Id, OwnerId FROM Account LIMIT 1];

        System.assert(updatedAccount.OwnerId == u.Id);
    }

}
