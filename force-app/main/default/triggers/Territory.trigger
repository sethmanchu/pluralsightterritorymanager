trigger Territory on Territory__c (after update) {
    // for testing purposes to run update functionality without a batch
    if (Trigger.isUpdate) {
        Territory_Handler.updateTerritoryOwnership(Trigger.newMap);
    }
}