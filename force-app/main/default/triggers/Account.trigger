trigger Account on Account (after update) {
    
    if (Trigger.isUpdate) {
        Account_Handler.syncAccountOwnerToTerritory(Trigger.newMap, Trigger.oldMap);
    }
}